#include "image.h"
#include <stdlib.h>

void rotate(struct image* source) {
    struct image rotated_image = create_image(source->height, source->width);
    for (uint64_t i = 0; i < rotated_image.height; i++)
        for (uint64_t j = 0; j < rotated_image.width; j++)
            rotated_image.data[i * rotated_image.width + j] = source->data[source->width * (j + 1) - i - 1];
    free(source->data);
    *source = rotated_image;
}
