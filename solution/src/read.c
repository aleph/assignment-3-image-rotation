#include "read.h"
#include "bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_TYPE 0x4D42
#define BMP_RESERVED 0
#define BMP_BI_SIZE 40
#define BMP_BI_PLANES 1
#define BMP_BI_BIT_COUNT 24
#define BMP_BI_COMPRESS 0
#define BMP_BI_PERLS_PER_METER 0
#define BMP_BI_CLR_USED 0
#define BMP_BI_CLR_IMPORTANT 0

static void initialize_header(struct bmp_header* header, uint64_t width, uint64_t height) {
    header->bfType = BMP_TYPE;
    header->bfileSize = sizeof(struct bmp_header) + width * height * sizeof(struct pixel);
    header->bfReserved = BMP_RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_BI_SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BMP_BI_PLANES;
    header->biBitCount = BMP_BI_BIT_COUNT;
    header->biCompression = BMP_BI_COMPRESS;
    header->biSizeImage = width * height * sizeof(struct pixel);
    header->biXPelsPerMeter = BMP_BI_PERLS_PER_METER;
    header->biYPelsPerMeter = BMP_BI_PERLS_PER_METER;
    header->biClrUsed = BMP_BI_CLR_USED;
    header->biClrImportant = BMP_BI_CLR_IMPORTANT;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;
    if (header.bfType != 0x4D42)
        return READ_INVALID_SIGNATURE;
    img->data = (struct pixel *)malloc(header.biWidth * header.biHeight * sizeof(struct pixel));
    for (uint32_t i = 0; i < header.biHeight; ++i) {
        if (fread(&img->data[i * header.biWidth], sizeof(struct pixel), header.biWidth, in) != header.biWidth) {
            free(img->data);
            return READ_INVALID_BITS;
        }
        fseek(in, (long)((4 - (header.biWidth * sizeof(struct pixel)) % 4) % 4), SEEK_CUR);
    }
    img->width = header.biWidth;
    img->height = header.biHeight;
    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    struct bmp_header header;
    initialize_header(&header, img->width, img->height);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;
    for (uint32_t i = 0; i < img->height; ++i) {
        if (fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;
        uint32_t padding = 0;
        fwrite(&padding, 1, (4 - (img->width * sizeof(struct pixel)) % 4) % 4, out);
    }
    return WRITE_OK;
}
