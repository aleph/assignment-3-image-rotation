#include "image.h"
#include "read.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    if (argc != 4) {
        fprintf(stderr, "Использование: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    const char* source_filename = argv[1];
    const char* transformed_filename = argv[2];
    int angle = atoi(argv[3]);

    if (angle % 90 != 0 || angle < -270 || angle > 270) {
        fprintf(stderr, "Неверный угол. Введите один из следующих углов: 0, 90, -90, 180, -180, 270, -270\n");
        return 1;
    }

    FILE* source_file = fopen(source_filename, "rb");
    if (source_file == NULL) {
        fprintf(stderr, "Не удалось открыть указанный файл\n");
        return 1;
    }

    struct image input_image;
    enum read_status read_result = from_bmp(source_file, &input_image);
    struct image rotated_image = input_image;
    fclose(source_file);

    if (read_result != READ_OK) {
        fprintf(stderr, "Не удалось прочитать изображение: %d\n", read_result);
        return 1;
    }

    int rotations = (angle % 360 + 360) % 360 / 90; // для отрицательных углов
    while (rotations > 0) {
        rotate(&rotated_image);
        rotations--;
    }

    FILE* transformed_file = fopen(transformed_filename, "wb");
    if (transformed_file == NULL) {
        fprintf(stderr, "Не удалось записать изображение в указанный файл\n");
        free(rotated_image.data);
        return 1;
    }

    enum write_status write_result = to_bmp(transformed_file, &rotated_image);
    fclose(transformed_file);
    free(rotated_image.data);

    if (write_result != WRITE_OK) {
        fprintf(stderr, "Не удалось записать изображение: %d\n", write_result);
        return 1;
    }

    return 0;
}
