#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height) {
    struct image img = {.width = width, .height = height};
    img.data = (struct pixel*)malloc(width * height * sizeof(struct pixel));
    if(!img.data) {
        fprintf(stderr, "Не удалось выделить память для изображения\n");
        exit(1);
    }
    return img;
}
