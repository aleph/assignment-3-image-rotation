#ifndef IMAGE_H
#define IMAGE_H

#include "pixel.h"
#include  <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height);

#endif
